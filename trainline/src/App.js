import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
const addr = 'https://api.comparatrip.eu/cities/';
let displayCalendar = 'none';
let displayCities = '';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value1: {
        unique_name: "",
      },
      value1modify: false,
      value2: {
        unique_name: "",
      },
      value2modify: false,
      value3: {
        unique_name: "",
      },
      popularCities: [],
      gareDepart: {},
      gareArrive: {},
      myYear: 0,
      myMonth: '',
      monthText: '',
      arrayDays: [],
      departDate: "",
      arriveDate: "",
      date1modify: false,
      date2modify: false,
    };
    this.onChange1 = this.onChange1.bind(this);
    this.onChange2 = this.onChange2.bind(this);
    this.onChange3 = this.onChange3.bind(this);
    this.citiesList = this.citiesList.bind(this);
    this.gareDepart = this.gareDepart.bind(this);
    this.getMyDate = this.getMyDate.bind(this);
    this.dateOnFocus = this.dateOnFocus.bind(this);
    /*     this.previousMonth = this.previousMonth.bind(this); */
  }

  getDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  };

  monthToString(month) {
    let monthTxt = "";
    switch (month) {
      case 1:
        monthTxt = "Janvier";
        break;
      case 2:
        monthTxt = "Février";
        break;
      case 3:
        monthTxt = "Mars";
        break;
      case 4:
        monthTxt = "Avril";
        break;
      case 5:
        monthTxt = "Mai";
        break;
      case 6:
        monthTxt = "Juin";
        break;
      case 7:
        monthTxt = "Juillet";
        break;
      case 8:
        monthTxt = "Août";
        break;
      case 9:
        monthTxt = "Septembre";
        break;
      case 10:
        monthTxt = "Octobre";
        break;
      case 11:
        monthTxt = "Novembre";
        break;
      case 12:
        monthTxt = "Décembre";
        break;
      default:
        break;
    }
    return monthTxt;
  }

  setDate(year, month) {
    let jourMois = this.getDaysInMonth(month, year);
    let i = 1;
    let arrayDate = [];
    while (i <= jourMois) {
      arrayDate.push(i);
      i++
    }
    this.setState({ arrayDays: arrayDate });
    this.setState({ myYear: year });
    this.setState({ myMonth: month });
    this.setState({ monthText: this.monthToString(month) })
  }

  componentDidMount() {
    axios.get(`${addr}/popular/5`)
      .then(res => {
        this.setState({ popularCities: res.data.slice(0, 5) });
      })
    let currentTime = new Date();
    let year = currentTime.getFullYear();
    let month = currentTime.getMonth() + 1;
    this.setDate(year, month);
  }

  onChange1(event) {
    let myValue1 = event.target.value;
    this.setState({
      value1: { unique_name: event.target.value }
    })
    if (event.target.value !== "") {
      axios.get(`${addr}/autocomplete/?q=${myValue1}`)
        .then(res => {
          this.setState({ popularCities: res.data.slice(0, 5) });
        })
    } else {
      axios.get(`${addr}/popular/5`)
        .then(res => {
          this.setState({ popularCities: res.data.slice(0, 5) });
        })
    }
  }
  onChange2(event) {
    this.setState({
      value2: { unique_name: event.target.value }
    })
  }
  onChange3(event) {
    this.setState({
      value3: { unique_name: event.target.value }
    })
  }

  gareDepart(city_id, local_name, unique_name) {
    if (this.state.value1modify === true) {
      this.setState({ value1: { unique_name } });
      this.gareArriveFocus.focus();
      axios.get(`${addr}/popular/from/${unique_name}/5`)
        .then(res => {
          this.setState({ popularCities: res.data.slice(0, 5) });
        })
      this.setState({ value1modify: false });
    } else if (this.state.value2modify === true) {
      this.setState({ value2: { unique_name } });
      this.maDateDepart.focus();
      axios.get(`${addr}/popular/from/${unique_name}/5`)
        .then(res => {
          this.setState({ popularCities: res.data.slice(0, 5) });
        })
      this.setState({ value2modify: false });
    }
  }

  citiesList(event) {
    displayCalendar = 'none';
    displayCities = '';
    if (event.target.name === "test1") {
      this.setState({ value1modify: true });
    } else if (event.target.name === "test2") {
      this.setState({ value2modify: true });
    }
  }

  getMyDate(day) {
    let month = this.state.myMonth;
    let year = this.state.myYear;

    if (this.state.date1modify === true) {
      this.setState({ departDate: `${day}/${month}/${year}` })
      this.setState({ date1modify: false });
      this.maDateArrive.focus();
    } else if (this.state.date2modify === true) {
      this.setState({ arriveDate: `${day}/${month}/${year}` })
      this.setState({ date2modify: false });
      this.monAge.focus();
    }

  }

  dateOnFocus(event) {
    displayCalendar = '';
    displayCities = 'none';
    if (event.target.name === "test3") {
      this.setState({ date1modify: true });
    } else if (event.target.name === "test4") {
      this.setState({ date2modify: true });
    }
  }
  /* 
    previousMonth(){
      let dweq =  isNaN(this.state.myMonth);
      console.log(dweq)
      let month = dweq;
      let year = this.state.myYear;
  
      this.setState({ myYear: year });
      this.setState({ myMonth: month })
  
      this.setDate(year,month);
    } */

  render() {
    return (
      <div className="App">
        <div className="div1">
          <div className="trajet">
            <label htmlFor="test1" style={{ textAlign: 'left' }}>Quel est votre trajet ?</label>
            <input placeholder="Saisissez votre gare de départ..." className="borderInputTrajet1" type="text" name="test1" value={this.state.value1.unique_name} onFocus={this.citiesList} onChange={this.onChange1} onKeyUp={this.onChange1}></input>
            <input placeholder="Saisissez votre gare d'arrivée..." className="borderInputTrajet2" type="text" name="test2" value={this.state.value2.unique_name} onFocus={this.citiesList} onChange={this.onChange2} ref={(input) => { this.gareArriveFocus = input; }} ></input>
          </div>
          <div className="date">
            <input placeholder="Aller" type="text" name="test3" className="borderInputTrajet3" defaultValue={this.state.departDate} onFocus={this.dateOnFocus} ref={(input) => { this.maDateDepart = input; }}></input>
            <input placeholder="Retour" type="text" name="test4" className="borderInputTrajet4" defaultValue={this.state.arriveDate} onFocus={this.dateOnFocus} ref={(input) => { this.maDateArrive = input; }}></input>
          </div>
          <div className="age">
            <input list="age" name="test5" ref={(input) => { this.monAge = input; }} />
            <datalist id="age">
              <option value="Jeune (0-25)" />
              <option value="Adulte (26-59)" />
              <option value="Senior (60+)" />
            </datalist>
            <button type="submit"> Submit </button>
          </div>
        </div>
        <div className="div2">
          <div className="citiesList" style={{ display: displayCities }}>
            {this.state.popularCities.map(({ local_name, city_id, unique_name }) => (
              <button type="submit" key={city_id} onClick={() => this.gareDepart(city_id, local_name, unique_name)}>{local_name}</button>
            ))}
          </div>
          <div className="calendrier" style={{ display: displayCalendar }}>
            <div className="month">
              <ul>
                <li onClick={this.previousMonth} className="prev">P</li>
                <li onClick={this.previousMonth} className="next">N</li>
                <li>{this.state.monthText}<span style={{ fontSize: 18 + 'px' }}><br />{this.state.myYear}</span></li>
              </ul>
            </div>

            <ul className="weekdays">
              <li>Mo</li>
              <li>Tu</li>
              <li>We</li>
              <li>Th</li>
              <li>Fr</li>
              <li>Sa</li>
              <li>Su</li>
            </ul>

            <ul className="days">
              {this.state.arrayDays.map((value) => (
                <li onClick={() => { this.getMyDate(value) }} key={value}>{value}</li>
              ))}

            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
